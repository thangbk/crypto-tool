package main

import (
	"bufio"
	"encoding/csv"
	"log"
	"os"
	"strings"
)

func getFilLines(filePath string) ([]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, strings.ToLower(scanner.Text()))
	}

	return lines, nil
}

func writeCsv(filePath string, records [][]string) error {

	f, err := os.Create(filePath)
	defer f.Close()

	if err != nil {
		log.Fatalln("Loi mo file csv:", err)
		return err
	}

	w := csv.NewWriter(f)
	err = w.WriteAll(records)

	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
