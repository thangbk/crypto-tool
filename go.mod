module gitlab.com/thangbk/crypto

go 1.12

require (
	github.com/ethereum/go-ethereum v1.10.13
	github.com/miguelmota/go-ethereum-hdwallet v0.1.1
	github.com/portto/solana-go-sdk v1.12.0
	github.com/tyler-smith/go-bip32 v1.0.0
	github.com/tyler-smith/go-bip39 v1.0.1-0.20181017060643-dbb3b84ba2ef
	github.com/urfave/cli v1.22.5
)
