package main
import (
	"github.com/urfave/cli"
	"log"
	"os"
	"strconv"
)

var (
	app *cli.App
)

func init() {
	app = cli.NewApp()
	app.Name = "Crypto Tool for Millennium Group"
	app.UsageText = "Update 2021-12-24 00:48"
	app.Usage = "Công cụ cho Airdrop, Whitelist"
	app.Authors = []cli.Author{
		{
			Name: "ThangBK",
			Email: "thangcnttbk@gmail.com",
		},
	}
	app.Version = "1.0.1"
}

func main() {
	app.Commands = []cli.Command{
		{
			Name:  "whitelist",
			Usage: "Kiểm tra kết quả làm Whitelist",
			Description: "Ví dụ: millennium whitelist wallet.txt whitelist.txt",
			UsageText: "millennium whitelist [File danh sách ví] [File danh sách whitelist]",
			Action: func(c *cli.Context) error {
				if c.Args().First() == "" {
					log.Fatalln("Sai tham số, gõ: \"millennium whitelist -h\" để xem hướng dẫn ")
					return nil
				}
				if err := CheckWhitelist(c.Args().Get(0), c.Args().Get(1)); err != nil {
					return cli.NewExitError(err.Error(), 1)
				}
				return nil
			},
		},
		{
			Name:  "wallet",
			Usage: "Tạo ví với số lượng lớn",
			Description: "Ví dụ: millennium wallet 24 10 vi.csv",
			UsageText: "millennium wallet [Số keyword] [Số lượng ví] [File CSV kết quả]",
			Action: func(c *cli.Context) error {
				if c.Args().First() == "" {
					log.Fatalln("Sai tham số, gõ: \"millennium wallet -h\" để xem hướng dẫn ")
					return nil
				}

				pharseSize, err := strconv.Atoi(c.Args().Get(0))
				if err != nil {
					log.Fatalln("Loi tham so: So keyword")
				}
				if pharseSize < 12 || pharseSize > 24 {
					log.Fatalln("So keyword khong hop le (12 =< n <= 24)")
				}
				count, err := strconv.Atoi(c.Args().Get(1))
				if err != nil {
					log.Fatalln("Loi tham so: So luong vi")
				}

				if err := walletToCsv(pharseSize, count, c.Args().Get(2)); err != nil {
					return cli.NewExitError(err.Error(), 1)
				}
				return nil
			},
		},{
			Name:  "wallet-solana",
			Usage: "Tạo ví Solana với số lượng lớn",
			Description: "Ví dụ: millennium wallet-solana 24 10 vi.csv",
			UsageText: "millennium wallet-solana [Số keyword] [Số lượng ví] [File CSV kết quả]",
			Action: func(c *cli.Context) error {
				if c.Args().First() == "" {
					log.Fatalln("Sai tham số, gõ: \"millennium wallet-solana -h\" để xem hướng dẫn ")
					return nil
				}

				pharseSize, err := strconv.Atoi(c.Args().Get(0))
				if err != nil {
					log.Fatalln("Loi tham so: So keyword")
				}
				if pharseSize < 12 || pharseSize > 24 {
					log.Fatalln("So keyword khong hop le (12 =< n <= 24)")
				}
				count, err := strconv.Atoi(c.Args().Get(1))
				if err != nil {
					log.Fatalln("Loi tham so: So luong vi")
				}

				if err := walletSolToCsv(pharseSize, count, c.Args().Get(2)); err != nil {
					return cli.NewExitError(err.Error(), 1)
				}
				return nil
			},
		},
		{
			Name:  "help",
			Usage: "Xem hướng dẫn sử dụng",
			Action: func(c *cli.Context) error {
				cli.ShowAppHelpAndExit(c, 1)
				return nil
			},
		},
	}

	_ = app.Run(os.Args)
}