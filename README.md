# Crypto Tool
Update 2021-12-24 00:45

Tool tiện ích về crypto, các chức năng sẽ được cập nhật liên tục.

- [x] Tạo ví số lượng lớn
- [x] Kiểm tra kết quả trúng Whitelist
- [ ] Tự động cài plugin Metamask cho chrome
- [ ] Chuyển tiền cho nhiều ví
- [ ] Thông báo khi token airdrop về ví
- [ ] Key License cho tool
- [ ] Thay đổi thông số trình duyệt
- [ ] Comming soon...

## Hướng dẫn tự build từ source code
##### MacOS
Mình code và build trên MacOS nên chỉ cần chạy lệnh:
```
go build -o crypto
```

##### Windows
```
GOOS=windows GOARCH=386 go build -o crypto.exe
```
hoặc
```
GOOS=windows GOARCH=amd64 go build -o crypto.exe
```

##### Ubuntu/Linux
```
GOOS=linux GOARCH=386 go build -o crypto
```
hoặc
```
GOOS=linux GOARCH=amd64 go build -o crypto
```
## Hướng dẫn sử dụng

AE có thể clone source code về và tự build để sử dụng hoặc có thể sử dụng 2 file mình đã build sẵn cho MacOS (crypto) và Windows (crypto.exe)

### Tạo ví số lượng lớn
Tool sử dụng thư viện thuật toán tạo ví của Ethereum, các ví tạo ra có thể import vào Metamask, Trust Wallet, C98,...

Tool sử dụng thư viện Go Ethereum và chạy hoàn toàn offline.

AE vào thư mục chứa file và mở CMD (Windows) hoặc Terminal (MacOS) tại thư mục đó và chạy lệnh sau:

##### MacOS

```
./crypto wallet 24 100 wallet.csv
```
##### Windows

```
./crypto.exe wallet 24 100 wallet.csv
```

##### Ubuntu/Linux
AE tự build hoặc cần build thì liên hệ mình build hộ

Trong đó:
+ 24: là số lượng keyword bạn muốn đặt làm khóa. Giá trị nằm trong khoảng 12 - 24
+ 100: là số lượng ví cần tạo
+ wallet.csv: là đường dẫn file chứa kết quả

##### Kết quả
Thông tin ví được tạo ra lưu ở file `wallet.csv` có nội dung dạng:
```
0x5Aaa55223AdD7d31295837B6d37165F4E8DEd6fA,025c98dc62c73708c5be99e7ffda878c068ec2334ece4d29fe70564b90d52083,west kingdom cover begin you ahead gym essence funny shadow pluck dragon
0x04e6fdb5CA9711212d60568952CAB04C88e81998,a65fb61ead23ac676dae6970919f88dda865955be31e1a6ba6cb94a9b4ed111c,wood virtual reflect intact unique ancient horror oppose old jealous chalk identify
0x290daECD9324FD94008335123f8aa631B9B151Ff,966d1ce279c520660212de9863ad26b1b11efc1820d9b2e4e6c0a44dfddec6c4,chair state motor reflect monster slender electric whip bean over wild plastic
0xf7F8895178A7334D4B13206708F5939394bE34E3,d90fe4b5c2178a11d52d36fd5e83f84a47722baff0dd16b500d9c259c517654f,pipe merry analyst fatigue teach bounce room erupt describe sustain income nose

```

AE mở file csv này bằng Excel rồi tách thành các cột cho dễ nhìn.


### Kiểm tra kết quả trúng Whitelist
Tool dùng để kiểm tra xem tập ví của mình có trúng trong danh sách Whitelist của dự án không?

##### MacOS

```
./crypto whitelist wallet.txt whitelist.txt
```
##### Windows

```
./crypto.exe whitelist wallet.txt whitelist.txt
```

Trong đó:
+ wallet.txt: là file chứa danh sách dịa chỉ ví tham gia làm whitelist
+ whitelist.txt: là file chứa danh sách địa chỉ ví trúng whitelist của dự án

Nội dung file `whitelist.txt` có thể là dạng đầy đủ ký tự hoặc dạng ẩn:
+ Dạng đầy đủ:
```
0xA5E46674A2E1058DCfDbE3Cbd8bbA1eFB8cE0B37
0xb73FAe9233Feb128a5665e90B0F76721D18f27bD
0xA9132d8e78ba0600752D262e11343aF23cDBB0B7
```
+ Dạng ẩn:
```
0xA5....0B37
0xb7....27bD
0xA9....B0B7
```

##### Kết quả
Thông tin danh sách ví trúng sẽ được hiện ra màn hình dạng:
```
Ban trung whitelist cac vi sau:
0x286B004bf627c6694B03c3c57BCd9a898754bFeB
0xeA012706cC89F0E708ba85A09315417024821adc
0x4fd93d210fC3B8902D815E247ac59Ce5588d4873
0x9dda5a40bF4d11e39Ce71FD903E3600432972344
0x9917B129F212119d36FB82E6b76641836a8B5d84

```
