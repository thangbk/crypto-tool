package main

import (
	"crypto/ed25519"
	"encoding/hex"
	hdwallet "github.com/miguelmota/go-ethereum-hdwallet"
	"github.com/portto/solana-go-sdk/types"
	"github.com/tyler-smith/go-bip39"
	"log"
)

func solWalletFromPhrase(phraseSize int) (string, string, string, error) {

	mnemonic, err := hdwallet.NewMnemonic(256)
	if err != nil {
		log.Fatalln(err)
	}
	seed64 := bip39.NewSeed(mnemonic, "")
	seed := seed64[:32]
	account, err := types.AccountFromSeed(seed)
	if err != nil {
		log.Fatalln(err)
	}

	privateKey := account.PrivateKey
	key := hex.EncodeToString(privateKey)

	publicKey, ok := privateKey.Public().(ed25519.PublicKey)
	if !ok {
		log.Fatalln("Could not assert the public key to ed25519 public key")
	}

	address := hex.EncodeToString(publicKey)
	return address, key, mnemonic, nil
}

func solWalletToCsv(pharseSize, count int, outputFile string) error {
	if pharseSize < 12 || pharseSize > 24 || len(outputFile) == 0 {
		log.Fatalln("Sai tham số, gõ: \"millennium wallet -h\" để xem hướng dẫn ")
	}
	records := make([][]string, 0)
	for i := 0; i < count; i++ {
		record := make([]string, 0)
		address, key, keyword, err := solWalletFromPhrase(pharseSize)
		if err != nil {
			log.Fatalln("Loi tao vi")
			return err
		}
		record = append(record, address, key, keyword)
		records = append(records, record)
	}

	return writeCsv(outputFile, records)
}