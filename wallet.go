package main

import (
	"encoding/hex"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/miguelmota/go-ethereum-hdwallet"
	"log"
)


func walletSolToCsv(pharseSize, count int, outputFile string) error {
	if pharseSize < 12 || pharseSize > 24 || len(outputFile) == 0 {
		log.Fatalln("Sai tham số, gõ: \"millennium wallet -h\" để xem hướng dẫn ")
	}
	records := make([][]string, 0)
	for i := 0; i < count; i++ {
		record := make([]string, 0)
		address, key, keyword, err := walletSolFromPhrase(pharseSize)
		if err != nil {
			log.Fatalln("Loi tao vi")
			return err
		}
		record = append(record, address, key, keyword)
		records = append(records, record)
	}

	return writeCsv(outputFile, records)
}

func walletToCsv(pharseSize, count int, outputFile string) error {
	if pharseSize < 12 || pharseSize > 24 || len(outputFile) == 0 {
		log.Fatalln("Sai tham số, gõ: \"millennium wallet -h\" để xem hướng dẫn ")
	}
	records := make([][]string, 0)
	for i := 0; i < count; i++ {
		record := make([]string, 0)
		address, key, keyword, err := walletFromPhrase(pharseSize)
		if err != nil {
			log.Fatalln("Loi tao vi")
			return err
		}
		record = append(record, address, key, keyword)
		records = append(records, record)
	}

	return writeCsv(outputFile, records)
}


func walletSolFromPhrase(phraseSize int) (string, string, string, error) {

	mnemonic, err := hdwallet.NewMnemonic(phraseSize*64/6)
	wallet, err := hdwallet.NewFromMnemonic(mnemonic)
	if err != nil {
		log.Fatal(err)
		return "", "", "", err
	}

	path := hdwallet.MustParseDerivationPath("m/44'/501'/0'")
	account, err := wallet.Derive(path, false)
	if err != nil {
		log.Fatal(err)
		return "", "", "", err
	}

	privateKey, err := wallet.PrivateKey(account)
	if err != nil {
		return "", "", "", err
	}

	key := hex.EncodeToString(privateKey.D.Bytes())
	address := crypto.PubkeyToAddress(privateKey.PublicKey).Hex()

	return address, key, mnemonic, nil
}

func walletFromPhrase(phraseSize int) (string, string, string, error) {

	mnemonic, err := hdwallet.NewMnemonic(phraseSize*64/6)
	wallet, err := hdwallet.NewFromMnemonic(mnemonic)
	if err != nil {
		log.Fatal(err)
		return "", "", "", err
	}

	path := hdwallet.MustParseDerivationPath("m/44'/60'/0'/0/0")
	account, err := wallet.Derive(path, false)
	if err != nil {
		log.Fatal(err)
		return "", "", "", err
	}

	privateKey, err := wallet.PrivateKey(account)
	if err != nil {
		return "", "", "", err
	}

	key := hex.EncodeToString(privateKey.D.Bytes())
	address := crypto.PubkeyToAddress(privateKey.PublicKey).Hex()

	return address, key, mnemonic, nil
}