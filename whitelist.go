package main

import (
	"log"
	"strings"
)

func CheckWhitelist(fileWallet, fileWhitelist string) error {
	if len(fileWallet) == 0 || len(fileWhitelist) == 0 {
		log.Fatalln("Sai tham số, gõ: \"millennium whitelist -h\" để xem hướng dẫn ")
	}

	wallets, err := getFilLines(fileWallet)
	if err != nil {
		log.Fatalln("Lỗi file wallet")
		return err
	}

	whitelist, err := getFilLines(fileWhitelist)
	if err != nil {
		log.Fatalln("Lỗi file whitelist")
		return err
	}

	if len(wallets) == 0 {
		log.Fatalln("File wallet rỗng")
		return nil
	}

	if len(whitelist) == 0 {
		log.Fatalln("Lỗi whitelist rỗng")
		return nil
	}

	resultMap := make(map[string]int)
	result := make([]string, 0)

	for _, v := range whitelist {
		if strings.Contains(v, ".") {
			elms := strings.Split(v, ".")
			for _, w := range wallets {
				isWhitelisted := true
				for _, e := range elms {
					if len(e) > 0 {
						if !strings.Contains(w, e) {
							isWhitelisted = false
						}
					}
				}

				if isWhitelisted {
					result = append(result, w)
				}
			}
		} else {
			resultMap[v] = 1
		}
	}

	if len(resultMap) > 0 {
		for _, v := range wallets {
			if _, ok := resultMap[v]; ok {
				result = append(result, v)
			}
		}
	}

	if len(result) == 0 {
		log.Println("Ban chua trung whitelist :(")
		return nil
	}

	log.Println("Ban trung whitelist cac vi sau:\n" + strings.Join(result, "\n"))
	return nil
}
